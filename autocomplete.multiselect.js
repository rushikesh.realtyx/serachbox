var selectedTerm = [];
var projectID = [];
var micromarketValues = [];

$.widget("ui.autocomplete", $.ui.autocomplete, {
    options : $.extend({}, this.options, {
        multiselect: false
    }),
    _create: function(){
        this._super();

        var self = this,
            o = self.options;

        if (o.multiselect) {
            // console.log('multiselect true');

            // console.log(self.o);

            self.selectedItems = {};           
            self.multiselect = $("<div></div>")
                .addClass("ui-autocomplete-multiselect ui-state-default ui-widget")
                .css("width", self.element.width())
                .insertBefore(self.element)
                .append(self.element)
                .bind("click.autocomplete", function(){
                    self.element.focus();
                });
            
            var fontSize = parseInt(self.element.css("fontSize"), 10);
            function autoSize(e){
                var $this = $(this);
                $this.width(1).width(this.scrollWidth+fontSize-1);
            };

            var kc = $.ui.keyCode;
            self.element.bind({
                "keydown.autocomplete": function(e){
                    if ((this.value === "") && (e.keyCode == kc.BACKSPACE)) {
                        var prev = self.element.prev();
                        delete self.selectedItems[prev.text()];
                        prev.remove();
                        selectedTerm = self.selectedItems
                        getSelectedItems(selectedTerm);
                    }
                },
                "focus.autocomplete blur.autocomplete": function(){
                    self.multiselect.toggleClass("ui-state-active");
                },
                "keypress.autocomplete change.autocomplete focus.autocomplete blur.autocomplete": autoSize
            }).trigger("change");

            o.select = function(e, ui) {
                $("<div></div>")
                    .addClass("ui-autocomplete-multiselect-item")
                    .text(ui.item.label)
                    .append(
                        $("<span></span>")
                            .addClass("ui-icon ui-icon-close")
                            .click(function(){
                                var item = $(this).parent();
                                delete self.selectedItems[item.text()];
                                item.remove();
                                selectedTerm = self.selectedItems
                                getSelectedItems(selectedTerm);
                            })
                    )
                    .insertBefore(self.element);
                
                self.selectedItems[ui.item.label] = ui.item;

                if ( ui.item.type == 'project' ) {
                    projectID.push(ui.item.id);
                } else {
                    micromarketValues.push(ui.item.id)
                }

                // console.log(projectID);

                // console.log(micromarketValues);
                

                
                self._value("");
                selectedTerm = self.selectedItems 
                getSelectedItems(selectedTerm, projectID, micromarketValues);
                return false;
            }
        }

        return this;
    }
});


// function getSelectedItems(selectedArray) {
//     console.log("selectedArray" + selectedArray);
//     $.ajax({
//         type: "POST",
//         url: "https://crematrix.herokuapp.com/core/v1/compare/",
//         dataType: "json",
//         data: {
//             project_ids: [147],
//             micromarkets: ["Worli"]
//         },
//         success: function (response) {
//             console.log(response);
//         }
//     })
// }

function getSelectedItems( selectedArray, projectID, micromarketValues ) {
    // console.log( selectedArray);    
    /**working */

    var settings = {
        "url": "https://crematrix.herokuapp.com/core/v1/compare/",
        "method": "POST",
        "timeout": 0,
        "headers": {
        "Content-Type": "application/json"
        },
        "data": JSON.stringify({ "project_ids": projectID, "micromarkets": micromarketValues }),
       // "data": JSON.stringify({ "project_ids": projectID, "micromarkets": ["Worli"] }),
       // "data": JSON.stringify({ "project_ids": [147], "micromarkets": ["Worli (Micromarket)"] }),
    };

    $.ajax(settings).done(function (response) {
        // console.log(JSON.parse(JSON.stringify(response.data.data)));
        // JSON.parse(JSON.stringify(response.data.data)).forEach(element => {
        //     console.log(element);
        // });

        var res = JSON.parse(JSON.stringify(response.data.data));
            
        var html = "";

        for (var elements in res ){
            console.log(res[elements]['Micromarket']);
            html += '<div class="col-4">';
            html += '<label for="">' + res[elements]['Complex Name'] + '</label>' +
                    '<label for="">' + res[elements]['Rera codes'] + '</label>' +
                    '<label for="">' + res[elements]['Micromarket'] + '</label>' +
                    '<label for="">' + res[elements]['Developer'] + '</label>' +
                    '<label for="">' + res[elements]['Price psf'] + '</label>' +
                    '<label for="">' + res[elements]['Total Registered'] + '</label>' +
                    '<label for="">' + res[elements]['type'] + '</label>' +
                    '<label for="">' + res[elements]['total_units'] + '</label>' +
                    '<label for="">' + res[elements]['total_booked']+'</label>';

            html += '</div >';
        }


        $("#result_div").append(html);

        // console.log(JSON.parse(JSON.stringify(response.data.data)).length)

        // $("#result_div").html(JSON.parse(JSON.stringify(response.data.data)))
    });
}


// var settings = {
//     "url": "https://crematrix.herokuapp.com/core/v1/compare/",
//     "method": "POST",
//     "timeout": 0,
//     "headers": {
//         "Content-Type": "application/json"
//     },
//     "data": JSON.stringify({ "project_ids": [147], "micromarkets": ["Worli"] }),
// };

// $.ajax(settings).done(function (response) {
//     console.log(response);
// });